### ezNmon-Manager



### ezNmon-Manager是[easyNmon](https://gitee.com/mzky/easyNmon)的WEB版多机部署工具和管理控制台，目前以war形式发布，自带sqlite数据库。丢到tomcat等容器的webapps目录下可直接使用。可远程批量操作多台机器的监控服务部署和基本性能实时监控。自动生成基于nmon的html和nmon原始文件下载。目标机在不需要安装任何语言环境和插件的情况下进行Linux系统资源监控。

### 下载地址：https://gitee.com/goodhal/ezNmon-Manager/releases


### 特别感谢[shadowedge](https://gitee.com/shadowedge)的ssh参考实现，使用本项目功能可以顺利实现。


![输入图片说明](https://images.gitee.com/uploads/images/2019/0911/085214_b5a6152c_393402.png "微信图片_20190911085157.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0911/085232_a360db34_393402.png "微信图片_20190911085158.png")

### 通过loadrunner启动监控和结束监控，示例：
创建一个新场景(与实际测试场景分离)，并将以下脚本内容稍作修改后放到init下（IP地址为需要监控的目标主机）：


```

web_custom_request("stop", //停止所有nmon监控
        "URL=http://192.168.126.166:8080/stop","Method=GET",LAST);
web_custom_request("start",
        "URL=http://192.168.126.166:8080/start?n=name&t=10",//n为场景名称，不支持中文；t为监控时长，单位分钟
        "Method=GET",LAST);
```



### 通过jmeter开启监控示例：

添加独立的线程组，添加仅一次控制器，在http request协议中填写如图参数即可；

![输入图片说明](https://images.gitee.com/uploads/images/2019/0911/103006_96e4c0b5_393402.jpeg "12.jpg")
注：独立线程组和仅一次控制器是为了避免重复执行

需要结束任务的，自己扩展一下~

